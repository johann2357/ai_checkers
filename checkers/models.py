import random

TYPES = {
    "MIN": 0,
    "MAX": 1,
}

CHECKERS = {
    "WHITE": 1,
    "BLACK": 2
}


class Tree(object):

    def __init__(self, board, max_depth):
        self.root = Node(
            TYPES["MAX"],
            0,
            GameState(board, TYPES["MAX"]),
            float("-inf"),
            float("inf"),
        )
        self.max_depth = max_depth

    def prune(self, alfa, beta, value, turn):
        if not turn:
            if value > beta:
                return True
            return False
        else:
            if value < alfa:
                return True
            return False

    def explore(self):
        self._explore(self.root)
        results = []
        for idx, x in enumerate(self.root.children):
            if self.root.value == x.value:
                results.append(idx)
        if results:
            return random.choice(results)
        return 0

    def _explore(self, node):
        if node.level > self.max_depth:
            return node.evaluation()
        for state in node.gameState.get_children():
            if self.prune(node.alpha, node.beta, node.value, node.turn):
                continue
            new_node = Node(
                int(not node.turn),
                node.level + 1,
                GameState(state, int(not node.turn)),
                node.alpha,
                node.beta,
            )
            node.insert(new_node)
            v = self._explore(new_node)
            if not node.turn:
                if v > node.value:
                    node.value = v
                node.alpha = node.value
            else:
                if v < node.value:
                    node.value = v
                node.beta = node.value
        return node.value

    def get_move(self):
        idx = self.explore()
        new_board = self.root.children[idx].gameState.board
        old_board = self.root.gameState.board
        result = []
        for i in xrange(8):
            for j in xrange(8):
                if new_board[i][j] != old_board[i][j]:
                    result.append([i, j])
        result = [result[0], result[-1]]
        return result


class Node(object):

    def __init__(self, _turn, _level, _gameState, alpha, beta):
        self.turn = _turn
        self.level = _level
        self.gameState = _gameState
        self.alpha = alpha
        self.beta = beta
        self.children = []
        if self.turn:
            self.value = float("inf")
        else:
            self.value = float("-inf")

    def insert(self, node):
        self.children.append(node)

    def evaluation(self):
        return self.gameState.evaluate()


class GameState(object):

    def __init__(self, _board, _turn):
        self.board = _board
        self.turn = _turn

    def evaluate(self):
        whites = 0
        blacks = 0
        for i in xrange(8):
            for j in xrange(8):
                if self.board[i][j] == CHECKERS["BLACK"]:
                    blacks += 1
                elif self.board[i][j] == CHECKERS["WHITE"]:
                    whites += 1
        if TYPES["MIN"] == self.turn:
            return whites - blacks
        if TYPES["MAX"] == self.turn:
            return blacks - whites

    def get_children(self):
        for i in xrange(8):
            for j in xrange(8):
                if (i + j) % 2:
                    if self.board[i][j] == CHECKERS["BLACK"]:
                        args = [(i, j), (i + 1, j + 1), CHECKERS["BLACK"]]
                        if self.is_move_legal(*args):
                            yield self.move(*args)

                        args = [(i, j), (i + 1, j - 1), CHECKERS["BLACK"]]
                        if self.is_move_legal(*args):
                            yield self.move(*args)

                        args = [(i, j), (i + 2, j + 2), CHECKERS["BLACK"]]
                        if self.is_move_legal(*args):
                            yield self.move(*args)

                        args = [(i, j), (i + 2, j - 2), CHECKERS["BLACK"]]
                        if self.is_move_legal(*args):
                            yield self.move(*args)

                    if self.board[i][j] == CHECKERS["WHITE"]:
                        args = [(i, j), (i - 1, j + 1), CHECKERS["WHITE"]]
                        if self.is_move_legal(*args):
                            yield self.move(*args)

                        args = [(i, j), (i - 1, j - 1), CHECKERS["WHITE"]]
                        if self.is_move_legal(*args):
                            yield self.move(*args)

                        args = [(i, j), (i - 2, j + 2), CHECKERS["WHITE"]]
                        if self.is_move_legal(*args):
                            yield self.move(*args)

                        args = [(i, j), (i - 2, j - 2), CHECKERS["WHITE"]]
                        if self.is_move_legal(*args):
                            yield self.move(*args)

    def make_editable(self, board):
        return map(list, list(board))

    def save_board(self, board):
        return tuple(map(tuple, board))

    def move(self, _from, _to, color):
        new_board = self.make_editable(self.board)
        fromRow = _from[0]
        fromCol = _from[1]
        toRow = _to[0]
        toCol = _to[1]

        new_board[toRow][toCol] = new_board[fromRow][fromCol]

        new_board[fromRow][fromCol] = 0
        # capture jumped piece
        if toRow == fromRow - 2:  # left direction
            if toCol == fromCol - 2:
                new_board[fromRow - 1][fromCol - 1] = 0
            else:
                new_board[fromRow - 1][fromCol + 1] = 0
        elif toRow == fromRow + 2:  # right direction
            if toCol == fromCol + 2:
                new_board[fromRow + 1][fromCol + 1] = 0
            else:
                new_board[fromRow + 1][fromCol - 1] = 0

        return self.save_board(new_board)

    def is_move_legal(self, _from, _to, color):
        if color != self.turn + 1:
            return False
        if _from == _to:
            return False
        fromRow = _from[0]
        fromCol = _from[1]
        toRow = _to[0]
        toCol = _to[1]

        if toRow not in range(8) or toCol not in range(8):
            return False

        if self.board[toRow][toCol] != 0:
            return False

        if color == CHECKERS["BLACK"]:
            if (toRow == (fromRow + 1)) and (
                    (toCol == (fromCol - 1))
                    or (toCol == (fromCol + 1))):
                return True

            if toRow == (fromRow + 2):
                if ((toCol == (fromCol - 2))
                        and (self.board[fromRow + 1][fromCol - 1] != 0)
                        and (self.board[fromRow + 1][fromCol - 1] != color)):
                    return True

                if ((toCol == fromCol + 2)
                        and (self.board[fromRow + 1][fromCol + 1] != 0)
                        and (self.board[fromRow + 1][fromCol + 1] != color)):
                    return True

        elif color == CHECKERS["WHITE"]:
            if ((toRow == fromRow - 1) and
                    ((toCol == fromCol - 1) or (toCol == fromCol + 1))):
                return True

            if toRow == (fromRow - 2):
                if ((toCol == (fromCol - 2))
                        and (self.board[fromRow - 1][fromCol - 1] != 0)
                        and (self.board[fromRow - 1][fromCol - 1] != color)):
                    return True
                if ((toCol == fromCol + 2)
                        and (self.board[fromRow - 1][fromCol + 1] != 0)
                        and (self.board[fromRow - 1][fromCol + 1] != color)):
                    return True
        return False
