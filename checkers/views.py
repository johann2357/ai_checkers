from django.shortcuts import render, HttpResponse
from .models import Tree
import json


def home(request):
    context = dict()
    return render(
        request,
        'index.html',
        context,
    )


def nextMove(request):
    board = json.loads(request.GET.get("board", [[]]))
    level = int(request.GET.get("level", 1))
    board = tuple(map(tuple, board))
    tree = Tree(board, level + 1)
    move = tree.get_move()
    resp = json.dumps({"move": move})
    return HttpResponse(resp, content_type="application/json")
