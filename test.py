board = [
    [0, 2, 0, 2, 0, 2, 0, 2],
    [2, 0, 2, 0, 2, 0, 2, 0],
    [0, 0, 0, 2, 0, 2, 0, 2],
    [0, 0, 2, 0, 0, 0, 0, 0],
    [0, 1, 0, 0, 0, 0, 0, 0],
    [0, 0, 1, 0, 1, 0, 1, 0],
    [0, 1, 0, 1, 0, 1, 0, 1],
    [1, 0, 1, 0, 1, 0, 1, 0]
]
board = tuple(map(tuple, board))
from checkers.models import *
from pprint import pprint
t = Tree(board, 5)
#   +1 +1
print t.root.gameState.is_move_legal([3, 2], [5, 0], CHECKERS["BLACK"])
# #   +1 -1
# print t.root.gameState.is_move_legal([2, 1], [3, 0], CHECKERS["BLACK"])
#
# #   +1 +1
# print t.root.gameState.is_move_legal([2, 3], [3, 4], CHECKERS["BLACK"])
# #   +1 -1
# print t.root.gameState.is_move_legal([2, 3], [3, 2], CHECKERS["BLACK"])
#
# #   +1 +1
# print t.root.gameState.is_move_legal([2, 5], [3, 6], CHECKERS["BLACK"])
# #   +1 -1
# print t.root.gameState.is_move_legal([2, 5], [3, 4], CHECKERS["BLACK"])
#
# #   +1 -1
# print t.root.gameState.is_move_legal([2, 7], [3, 6], CHECKERS["BLACK"])

# for x in t.root.gameState.get_children():
#     pprint(x)
#     gs = GameState(x, 0)
#     for y in gs.get_children():
#         pprint(y)
#

t.get_move()
# print t.root.children
# print len(t.root.children)
# print t.root.value
# for x in t.root.children:
#     print x.value
for x in t.root.gameState.get_children():
    pprint(x)
