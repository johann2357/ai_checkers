from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    # Examples:
    # url(r'^$', 'ai_checkers.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', 'checkers.views.home', name='home'),
    url(r'^api/nextMove$', 'checkers.views.nextMove', name='nextMove'),
    url(r'^admin/', include(admin.site.urls)),
]

urlpatterns += staticfiles_urlpatterns()
